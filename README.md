[[_TOC_]]

# The su_apache Puppet module

## Overview

This Puppet module

* Installs Apache 2.4

* Opens up the iptables firewall to allow access to ports 80 and 443 from the entire internet

* Manages the Apache SSL security settings files

## Configuration

The `su_apache` module will install the filter-syslog file apropriate for
Apache. If you want do not want to install this file, set
`su-apache::filter_syslog` to `absent` in your Hiera file(s).

### Debian configuration

### RedHat configuration
