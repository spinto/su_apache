# Apache configuration for Debian.

# Only works for Debian stretch and later.

#######################################################################
# An SSL cipher specification in cipher-spec is composed of 4 major
# attributes plus a few extra minor ones:
#
#    Key Exchange Algorithm:
#      RSA, Diffie-Hellman, Elliptic Curve Diffie-Hellman,
#      Secure Remote Password
#
#    Authentication Algorithm:
#      RSA, Diffie-Hellman, DSS, ECDSA, or none.
#
#    Cipher/Encryption Algorithm:
#      AES, DES, Triple-DES, RC4, RC2, IDEA, etc.
#
#    MAC Digest Algorithm:
#      MD5, SHA or SHA1, SHA256, SHA384.
#######################################################################


# $tls_protocols: an array of SSL/TLS protocols that will be used with the
# Apache SSLProtocol directive. Use this ONLY if you want to override the
# normal SSL/TLS protocols chosen based on the major version of Debian.

# $enable_RC4: normally RC4-based ciphers are not enabled. Set this parameter
# to "true" if you _really_ need to enable RC4.
#
# $enable_3DES: normally 3DES-based ciphers are not enabled. Set this parameter
# to "true" if you need to enable RC4.

# $enable_RSA: enable all ciphers using RSA key exchange.

class su_apache::debian (
  Array[String] $tls_protocols   = [],
  #
  Boolean       $enable_RC4      = false,
  Boolean       $enable_3DES     = false,
  #
  Boolean       $enable_RSA      = true,
){
  # Note: the readlink executable comes from the coreutils package.
  exec { 'a2dissite 000-default.conf':
    command => 'a2dissite 000-default.conf',
    onlyif  => 'readlink /etc/apache2/sites-enabled/000-default.conf',
    path    => '/bin:/usr/bin:/usr/sbin',
    require => Package['apache'],
  }

  $debian_major_version = $facts['os']['distro']['release']['major']

  if ($facts['os']['name'] == 'Debian') {
    if (Integer($debian_major_version) < 9) {
      fail("This class only works with Debian stretch and later")
    }
  } elsif ($facts['os']['name'] == 'Ubuntu') {
    if (Float($debian_major_version) < 16.0) {
      fail("This class only works with Ubuntu xenial and later")
    }
  }

  # Set the SSL/TLS protocols and ciphers.
  su_apache::conf { 'ssl-strength.conf':
    ensure  => present,
    content => template('su_apache/etc/apache2/conf.d/ssl-strength.erb'),
  }

  # Replace the default Apache security configuration file with one that
  # suppresses most information disclosure about the server.
  su_apache::conf { 'security.conf':
    ensure => present,
    source => 'puppet:///modules/su_apache/etc/apache2/conf.d/security',
    require => Package['apache'],
    notify  => Service['apache'],
  }

  # Support /server-status for all virtual hosts, but only from localhost.
  su_apache::module { 'status':
    ensure  => present,
    require => File['/etc/apache2/mods-available/status.conf'],
  }
  file { '/etc/apache2/mods-available/status.conf':
    source  => 'puppet:///modules/su_apache/etc/apache2/mods-available/status.conf',
    require => Package['apache'],
    notify  => Service['apache'],
  }
}
