# Load the package and set the "apache" package resource.

class su_apache::package {

  if ($::osfamily == 'RedHat') {
    package { 'apache':
      ensure => present,
      name   => 'httpd',
    }
  } elsif ($::osfamily == 'Debian') {
    if (($::lsbdistcodename == 'wheezy') or ($::lsbdistcodename == 'jessie')) {
      package { 'apache':
        ensure => present,
        name   => 'apache2-mpm-prefork',
      }
    } else {
      package { 'apache':
        ensure => present,
        name   => 'apache2',
      }
    }
  } else {
    crit('do not know what to do')
  }

}
