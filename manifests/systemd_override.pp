# Override systemd's apache configuration.

# $private_tmp: if set to true then systemd will force Apache to use a
# "private" /tmp directory (that is, when Apache writes to /tmp/ it is in
# fact writing to a special directory in /var/tmp that no other process
# has access to). This is a problem for certreq since certreq creates a
# file in /tmp and then calls an external executable to get NetDB access
# information that needs to access this file. A private /tmp makes this
# impossible. So we set it to false by default.

class su_apache::systemd_override (
  $private_tmp = false
) {

  systemd::dropin_file { 'apache2.conf':
    unit    => 'apache2.service',
    content => template('su_apache/systemd-dropin/apache2.conf.erb'),
  }

}
