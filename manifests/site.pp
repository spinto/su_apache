# Install an Apache site configuration and enable it. Meant to be used with the
# puppetlabs/apache module.
#
# This is used for virtual hosts and enables the site with the Apache a2ensite
# tool.  Examples:
#
#     apache::site {
#       'www':
#         source => 'puppet:///modules/s_www/etc/apache2/sites-available/www';
#       'proxy':
#         content => template('s_www/proxy.erb')
#     }
#
# This infrastructure, and therefore this define, is only available on Debian.

define su_apache::site(
  Enum['present', 'absent'] $ensure  = undef,
  Optional[String]          $source  = undef,
  Optional[String]          $content = undef,
) {
  if ($::operatingsystem != 'debian') and ($::operatingsystem != 'ubuntu') {
    fail("Unsupported apache::site operating system ${::operatingsystem}")
  }

  # With jessie (and later) the files controlled by a2ensite must have a
  # suffix of '.conf'.
  if (($::lsbdistcodename != 'wheezy') and ($name !~ /.conf$/)) {
    $site_id  = "${name}.conf"
    $realname = "/etc/apache2/sites-available/${name}.conf"
    file {
      "/etc/apache2/sites-available/${name}": ensure => absent;
      "/etc/apache2/sites-enabled/${name}":   ensure => absent;
    }
  } else {
    $site_id  = $name
    $realname = "/etc/apache2/sites-available/${name}"
  }

  # Install the site configuration.
  file { $realname:
    ensure  => $ensure,
    source  => $source,
    content => $content,
    notify  => Exec['su_apache_reload'];
  }

  # Enable or disable the site, based on the ensure parameter.
  if ($ensure == 'present') {
    exec { "a2ensite ${site_id}":
      command => "a2ensite ${site_id}",
      path    => '/usr/bin:/usr/sbin',
      creates => "/etc/apache2/sites-enabled/${site_id}",
      require => [ Package['apache'], File[$realname] ],
      notify  => Exec['su_apache_reload'],
    }
  } elsif ($ensure == 'absent') {
    exec { "a2dissite ${site_id}":
      command => "a2dissite ${site_id}",
      unless  => "[ ! -e '/etc/apache2/sites-enabled/${site_id}' ]",
      path    => '/usr/bin:/usr/sbin',
      require => Package['apache'],
      notify  => Exec['su_apache_reload'],
    }
  }
}
