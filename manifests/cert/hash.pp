# Creates the OpenSSL hash symlinks for certificates. Factored out for
# ensure handling and since it's used for both main certificates and
# root certificates.

define su_apache::cert::hash(
  Enum['present', 'absent'] $ensure    = 'present',
  String                    $directory = '/etc/ssl/certs',
) {

  $hashcommand = "`openssl x509 -noout -hash -in ${directory}/${name}`"

  # Create the link if ensure is present.
  if ($ensure == 'present') {
    exec { "openssl hash link ${directory}/${name}":
      command => "ln -s ${directory}/${name} ${directory}/${hashcommand}.0",
      path    => ['/usr/bin', '/usr/sbin'],
      unless  => "[ -f \"${directory}/${hashcommand}.0\" ]",
      require => $::osfamily ? {
        'Debian' => [ File["${directory}/${name}"],
                      Package['ca-certificates'],
                      Package['openssl'] ],
        'RedHat' => [ File["${directory}/${name}"], Package['openssl'] ],
      },
    }
  }
}
