# Create a self_signed certificate, normally for use with Apache.
#
# su_apache::cert::self_signed creates a self_signed certificate with a long
# expiration time if one doesn't already exist.  The certificate will, by
# default, be symlinked to /etc/ssl/certs/server.pem and the key to
# /etc/ssl/private/server.key.
#
# Syntax:
#
#     su_apache::cert::self_signed { "<hostname>":
#         ensure     => present,
#         identity   => "<hostname>.stanford.edu",
#         pool       => false,
#         serverlink => true,
#         certdir    => '/etc/ssl/certs',
#         keydir     => '/etc/ssl/private',
#     }
#
# Only ensure needs to be specified.  All other parameters default to the
# values given above.  There isn't a way to set the ownership of a certificate
# generated with su_apache::cert::self_signed currently; it will always be
# root:ssl-cert, with the key mode 0640.  Lifetime for a self_signed
# certificate will always be ten years.
#
# The certificate identity can be overridden with the identity parameter.
#
# If pool is true, make-local-cert is invoked with -p to generate an identity
# based on the local hostname with trailing numbers stripped off.
#
# If serverlink is false, make-local-cert is invoked with -m which suppresses
# the creation of symlinks to server.key and server.pem.  This allows multiple
# certificates to be generated on the same system.  To create multiple
# certificates on the same system the file parameter must also be specified.
#
# The default directories into which to put the certificate and the private
# key can be overridden with certdir and keydir.  The file name cannot be
# changed and will always be the domain-stripped identity with .pem and .key
# extensions.  Use a symlink if it needs to be a different name.  To use these
# parameters, stanford-server 69 (Debian version) or later is required.

define su_apache::cert::self_signed(
  $ensure,
  $identity   = $fqdn,
  $pool       = false,
  $serverlink = true,
  $certdir    = '/etc/ssl/certs',
  $keydir     = '/etc/ssl/private'
) {
  if ($ensure != 'present') and ($ensure != 'absent') {
    fail("Invalid ensure value: $ensure")
  }

  # Currently, do nothing if ensure is absent.
  if $ensure == 'present' {
    include su_apache::cert::packages

    $pflag = $pool       ? { true  => '-p', default => '' }
    $mflag = $serverlink ? { false => '-m', default => '' }
    if ($certdir != '/etc/ssl/certs') or ($keydir != '/etc/ssl/private') {
      $flags = "-c '${certdir}' -k '${keydir}' ${pflag} ${mflag}"
    } else {
      $flags = "${pflag} ${mflag}"
    }
    $filename = regsubst($identity, '^([^.]+)\..*', '\1.pem')
    exec { "make-local-cert ${identity}":
      command => "/usr/sbin/make-local-cert ${flags} ${identity}",
      creates => "${certdir}/${filename}",
      require => $::osfamily ? {
        'Debian' => [ Package["ca-certificates"],
                      Package["openssl"],
                      Package["stanford-server"] ],
        'RedHat' => [ Package["openssl"],
                      Package["stanford-server"],
                      File["/etc/ssl/certs"] ],
      },
    }
  }
}
