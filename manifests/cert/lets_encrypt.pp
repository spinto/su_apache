# (The idea for this module comes from Alex Tayts.)
#
# Link to an EXISTING Let's Encrypt certificate (if the certificate
# exists). Otherwise, link to a "snake-oil" certificate so that Apache
# works.
#
# 1. If the Let's Encrypt certificate does NOT exist, then be sure
#    the snake-oil certificate exists and link to it.
#
# 2. If the Let's Encrypt certificate DOES exist, then link to it.
#
# $name: the fully-qualified domain name of the Subject.
# Example: debian-repo-dev.stanford.edu. This MUST match the
# cert-name of the Let's Encrypt certificate, i.e., must match
# the directory /etc/letsencrypt/live/<FQDN>.

define su_apache::cert::lets_encrypt {

  include su_apache::cert::link_lets_encrypt_script

  # The link-lets-encrypt-certs script returns 0 only when some
  # linking occurs.
  exec { "link-lets-encrypt-${name}":
    path    => '/usr/bin:/usr/sbin',
    command => "echo 'linked certificates'",
    onlyif  => "/usr/sbin/link-lets-encrypt-certs $name",
    require => File['/usr/sbin/link-lets-encrypt-certs'],
  }

}
