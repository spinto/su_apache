# Install a root certificate. Creates the hash symlink.
#
# This define assumes that both the openssl and ca-certificates
# package Puppet resources have been defined elsewhere; if not,
# raise a Puppet error.

define su_apache::cert::root(
  Enum['present', 'absent'] $ensure = 'present'
) {

  # Make sure the ca-certificates package is installed.
  ensure_packages(['ca-certificates', 'openssl'], {ensure => present})

  file { "/etc/ssl/certs/${name}.pem":
    ensure  => $ensure,
    source  => "puppet:///modules/su_apache/etc/ssl/certs/${name}.pem",
    require => $::osfamily ? {
      'Debian' => Package['ca-certificates'],
      'RedHat' => $::lsbmajdistrelease ? {
        '6'     => Package['ca-certificates'],
        default => Package['openssl'],
      },
    },
  }

  su_apache::cert::hash { "${name}.pem": ensure => $ensure }
}
