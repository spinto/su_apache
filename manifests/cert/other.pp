# This used to be a separate class for certificates that are not self-signed
# but were not from Comodo.  However, su_apache::cert::comodo no longer does
# anything Comodo-specific, so this can be a simple wrapper.
#
# Eventually, both classes should be merged and renamed.

define su_apache::cert::other(
  $ensure,
  $keyname     = undef,
  $owner       = 'root',
  $group       = undef,
  $identity    = undef,
  $symlink     = true
) {
  su_apache::cert::comodo { $name:
    ensure   => $ensure,
    keyname  => $keyname,
    owner    => $owner,
    group    => $group,
    identity => $identity,
    symlink  => $symlink,
  }
}
