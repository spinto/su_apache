# Install an InCommon-provided certificate.

# The key is downloaded via wallet.  The certificate will be symlinked to
# /etc/ssl/certs/server.pem and the key to /etc/ssl/private/server.key.

# The root/intermediate certificate chain is installed as the bundle

# Syntax:
#
#     su_apache::cert::incommon { "<hostname>":
#         ensure      => present,
#         keyname     => "ssl-key/<hostname>",
#         owner       => "root",
#         group       => $operatingsystem ? {
#                            debian => "ssl-cert",
#                            ubuntu => "ssl-cert",
#                            redhat => "root",
#                        },
#         mode        => "0640",
#         identity    => "<hostname>",
#         symlink     => false,
#     }
#
# <hostname> MUST be full-qualified.
#
# Only ensure need be specified; the other listed parameters are the defaults.
# <hostname> should be the unqualified hostname.  The public certificate
# should be stored in modules/apache/files/certs/<hostname> (defaulting to
# <hostname>).

########################################################################################

# $name: This should be the FULLY qualified name. For example "example.stanford.edu". You can
# also provide something like "example.stanford.edu-saml" for a SAML key-pair. $name implies
# the wallet name and certificate filename:
#
#    wallet name -> "ssl-key/$name"
#    cert_files  -> $name
#
# Example. If $name is "example.stanford.edu-saml" then the wallet name will be
# "ssl-key/example.stanford.edu-saml" and the certificate pulled from the cert_files
# module will be "example.stanford.edu-saml".
#
# $ensure: set to present to install the certificate, absent to uninstall. This parameter
# is required and defaults to 'present'.
#
# $keyname: the Wallet file object name. Defaults to "ssl-key/$name"
#
# $owner: file owner of the private key. Defaults to "root".
#
# $group: group owner of the private key. Default will depend on the OS.
#
# $identity: If the file name in the cert_files module does NOT match $name, you can specify
# the file name with $identity. Example:
#
# su_apache::cert::incommon { 'example.stanford.edu':
#   identity => 'example2.stanford.edu',
# }
#
# This will install the certificate "example.stanford.edu" but pull use the certificate file
# "example2.stanford.edu" from the cert_files module.
#
# $symlink: if set to true will create a symlink from the cert and private key to the filenames
# "server.pem" and "server.key", respectively. Defaults to false.

########################################################################################

define su_apache::cert::incommon(
  Enum['present', 'absent'] $ensure   = 'present',
  Optional[String]          $keyname  = undef,
  String                    $owner    = 'root',
  Optional[String]          $group    = undef,
  String                    $mode     = '0640',
  Optional[String]          $identity = undef,
  Boolean                   $symlink  = false,
) {

  # Include the InCommon root/intermediate chain bundle.
  if ($ensure == 'present') {
    include su_apache::cert::root::incommon
  }

  # Install the private key using Wallet.
  case $keyname {
    undef:   { $key = "ssl-key/${name}" }
    default: { $key = $keyname }
  }

  # Calculate the group name.
  $group_name = $group ? {
    undef   => $::operatingsystem ? {
      'debian' => 'ssl-cert',
      'ubuntu' => 'ssl-cert',
      'redhat' => 'root',
      'CentOS' => 'root',
    },
    default => $group,
  }

  base::wallet { $key:
    ensure  => $ensure,
    type    => 'file',
    path    => "/etc/ssl/private/${name}.key",
    owner   => $owner,
    group   => $group_name,
    mode    => $mode,
    require => $::osfamily ? {
      'Debian' => Package['ca-certificates'],
      'RedHat' => $::lsbmajdistrelease ? {
        '6'     => Package['ca-certificates'],
        default => Package['openssl'],
      },
    },
  }

  # Install the public certificate.
  file { "/etc/ssl/certs/${name}.pem":
    ensure  => $ensure,
    source  => $identity ? {
      undef   => "puppet:///modules/cert_files/${name}",
      default => "puppet:///modules/cert_files/${identity}",
    },
    require => $::osfamily ? {
      'Debian' => Package['ca-certificates'],
      'RedHat' => $::lsbmajdistrelease ? {
        '6'     => Package['ca-certificates'],
        default => Package['openssl'],
      },
    },
    notify  => Service['apache'],
  }

  # Create the OpenSSL hash links.
  su_apache::cert::hash { "${name}.pem": ensure => $ensure }

  # Install the server symlinks unless symlink is set to false.
  if ($symlink) {
    file { '/etc/ssl/certs/server.pem':
      ensure  => $ensure ? {
        present => link,
        absent  => absent,
        default => $ensure,
      },
      target  => "${name}.pem",
      require => $::osfamily ? {
        'Debian' => Package['ca-certificates'],
        'RedHat' => $::lsbmajdistrelease ? {
          '6'     => Package['ca-certificates'],
          default => Package['openssl'],
        },
      },
    }

    file { '/etc/ssl/private/server.key':
      ensure => $ensure ? {
        present => link,
        absent  => absent,
        default => $ensure,
      },
      target => "${name}.key",
      require => $::osfamily ? {
        'Debian' => Package['ca-certificates'],
        'RedHat' => $::lsbmajdistrelease ? {
          '6'     => Package['ca-certificates'],
          default => Package['openssl'],
        },
      },
    }
  }
}
