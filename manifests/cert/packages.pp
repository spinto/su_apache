# Used to load required packages and install Comodo root certificates for the
# su_apache::cert::* defines from one class so that we don't have multiple
# definition problems.

# Helper define to install a Comodo root certificate.
define su_apache::cert::packages::root($ensure) {
  file { "/etc/ssl/certs/${name}.pem":
    ensure  => $ensure,
    source  => "puppet:///modules/su_apache/etc/ssl/certs/${name}.pem",
    require => $::osfamily ? {
      'Debian' => Package['ca-certificates'],
      'RedHat' => $::lsbmajdistrelease ? {
        '6'     => Package['ca-certificates'],
        default => [ Package['openssl'], File['/etc/ssl/private'] ],
      },
    },
  }
  su_apache::cert::hash { "${name}.pem": ensure => $ensure }
}

class su_apache::cert::packages {
  package { 'openssl': ensure => present }

  # Install the basic certificate handling infrastructure.
  case $::osfamily {
    'Debian': {
      package { 'ca-certificates': ensure => present }
    }

    # The ca-certificates package on Debian and RHEL6 provides this, but no
    # package on Red Hat does.
    'RedHat': {
      file {
        '/etc/ssl':
          ensure  => directory,
          mode    => '0755';
        '/etc/ssl/certs':
          ensure  => directory,
          mode    => '0755';
        '/etc/ssl/private':
          ensure  => directory,
          mode    => '0710';
      }

      if ($::lsbmajdistrelease > 5) {
        package { 'ca-certificates': ensure => present }
      }
    }

    default: {
      crit("cannot handle operating system${::operatingsystem}")
    }

  }

  # Install all the known Comodo intermediate certificates.
  su_apache::cert::packages::root {
    [
      'comodo-addtrust-2020',
      'comodo-addtrust-2020-new',
      'comodo-entrust-2015',
      'comodo-entrust-2019',
      'incommon-addtrust-2020',
      'addtrust-usertrust-2020',
      'incommon-usertrust-2024',
    ]: ensure => present;
  }

  # Install a copy of the current Comodo certificate bundle.  This is only
  # used by other manifests as a trust anchor and is not hashed, since hashing
  # certificates is only meaningful for files containing a single certificate
  # and both certificates in this file are also provided elsewhere.
  file { '/etc/ssl/certs/comodo-incommon-addtrust-bundle-2020.pem':
    source  => 'puppet:///modules/su_apache/etc/ssl/certs/comodo-incommon-addtrust-bundle-2020.pem',
    require => $::osfamily ? {
      'Debian' => Package['ca-certificates'],
      'RedHat' => $::lsbmajdistrelease ? {
        '6'     => Package['ca-certificates'],
        default => [ Package['openssl'], File['/etc/ssl/private'] ],
      },
    },
  }

  # [Added 30-Sep-2014] Install a copy of the newest Comodo certificate
  # bundle.
  file { '/etc/ssl/certs/incommon-usertrust-addtrust-bundle-2020.pem':
    source  => 'puppet:///modules/su_apache/etc/ssl/certs/incommon-usertrust-addtrust-bundle-2020.pem',
    require => $::osfamily ? {
      'Debian' => Package['ca-certificates'],
      'RedHat' => $::lsbmajdistrelease ? {
        '6'     => Package['ca-certificates'],
        default => [ Package['openssl'], File['/etc/ssl/private'] ],
      },
    },
  }
}
