class su_apache::cert::link_lets_encrypt_script {

  file { '/usr/sbin/link-lets-encrypt-certs':
    ensure => present,
    source => 'puppet:///modules/su_apache/usr/sbin/link-lets-encrypt-certs',
    mode   => '0755',
  }

}
