# Install the required InCommon root and intermediate certificates.
class su_apache::cert::root::incommon {

  # Install all the known Comodo and InCommon intermediate certificates.
  su_apache::cert::root {
    [
      'incommon-usertrust-2024',
    ]: ensure => present;
  }

  # [Added 30-Jan-2020] Install a copy of the newest Comodo certificate
  # bundle with the 2024 InCommon intermediate and the new 2038 UserTrust
  # root certificate. See also
  # https://ikiwiki.stanford.edu/service/certreq/incommon2020/
  file { '/etc/ssl/certs/incommon2024-usertrust2038-bundle.pem':
    source  => 'puppet:///modules/su_apache/etc/ssl/certs/incommon2024-usertrust2038-bundle.pem',
    require => $::osfamily ? {
      'Debian' => Package['ca-certificates'],
      'RedHat' => $::lsbmajdistrelease ? {
        '6'     => Package['ca-certificates'],
        default => Package['openssl'],
      },
    },
  }

}
