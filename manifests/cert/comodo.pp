# Install a Comodo-provided certificate.  The root certificate is installed as
# to be part of the CA chain and the key is pulled from Puppet.  The
# certificate will be symlinked to /etc/ssl/certs/server.pem and the key to
# /etc/ssl/private/server.key.
#
# Syntax:
#
#     su_apache::cert::comodo { "<hostname>":
#         ensure      => present,
#         keyname     => "unix-<hostname>-ssl-key",
#         owner       => "root",
#         group       => $operatingsystem ? {
#                            debian => "ssl-cert",
#                            ubuntu => "ssl-cert",
#                            redhat => "root",
#                        },
#         mode        => 640,
#         identity    => "<hostname>.stanford.edu",
#         comodoroot  => "entrust-2012",
#         symlink     => true,
#         installroot => true,
#     }
#
# Only ensure need be specified; the other listed parameters are the defaults.
# <hostname> should be the unqualified hostname.  The public certificate
# should be stored in modules/apache/files/certs/<identity> (defaulting to
# <hostname>.stanford.edu).
#
# comodoroot and installroot are accepted for backward compatibility but
# ignored.  All known Comodo roots are automatically installed by this define
# (through the use of the included apache::cert::packages class).

# The default is "group = ssl-cert" which works for Debian, not on RHEL, so we
# handle that below.
define su_apache::cert::comodo(
  $ensure,
  $keyname     = undef,
  $owner       = 'root',
  $group       = undef,
  $mode        = '0640',
  $identity    = undef,
  $comodoroot  = '',
  $symlink     = true,
  $installroot = true
) {
  if !($ensure in [ 'present', 'absent' ]) {
    fail("ensure must be present or absent, not $ensure")
  }

  # Include required packages.
  if ($ensure == 'present') {
    include su_apache::cert::packages
  }

  # Install the private key.
  case $keyname {
    undef:   { $key = "unix-${name}-ssl-key" }
    default: { $key = $keyname }
  }
  base::wallet { $key:
    ensure  => $ensure,
    type    => 'file',
    path    => "/etc/ssl/private/${name}.key",
    owner   => $owner,
    group   => $group ? {
      undef   => $::operatingsystem ? {
        'debian' => 'ssl-cert',
        'ubuntu' => 'ssl-cert',
        'redhat' => 'root',
        'CentOS' => 'root',
      },
      default => $group,
    },
    mode    => $mode,
    require => $::osfamily ? {
      'Debian' => Package['ca-certificates'],
      'RedHat' => $lsbmajdistrelease ? {
        '6'     => Package['ca-certificates'],
        default => [ Package['openssl'],
                     File['/etc/ssl/private'] ],
      },
    },
  }

  # Install the public certificate.
  file { "/etc/ssl/certs/${name}.pem":
    ensure  => $ensure,
    source  => $identity ? {
      undef   => "puppet:///modules/cert_files/${name}.stanford.edu",
      default => "puppet:///modules/cert_files/$identity",
    },
    require => $::osfamily ? {
      'Debian' => Package['ca-certificates'],
      'RedHat' => $lsbmajdistrelease ? {
        '6'     => Package['ca-certificates'],
        default => [ Package['openssl'],
                     File['/etc/ssl/private'] ],
      },
    },
  }

  # Create the OpenSSL hash links.
  su_apache::cert::hash { "${name}.pem": ensure => $ensure }

  # Install the server symlinks unless symlink is set to false.
  if ($symlink == true) {
    file { '/etc/ssl/certs/server.pem':
      ensure  => $ensure ? {
        present => link,
        absent  => absent,
        default => $ensure,
      },
      target  => "${name}.pem",
      require => $::osfamily ? {
        'Debian' => Package['ca-certificates'],
        'RedHat' => $lsbmajdistrelease ? {
          '6'     => Package['ca-certificates'],
          default => [ Package['openssl'],
                       File['/etc/ssl/private'] ],
        },
      },
    }
    file { '/etc/ssl/private/server.key':
      ensure => $ensure ? {
        present => link,
        absent  => absent,
        default => $ensure,
      },
      target => "${name}.key",
      require => $osfamily ? {
        'Debian' => Package['ca-certificates'],
        'RedHat' => $lsbmajdistrelease ? {
          '6'     => Package['ca-certificates'],
          default => [ Package['openssl'],
                       File['/etc/ssl/private'] ],
        },
      },
    }
  }
}
