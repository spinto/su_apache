# Enable an Apache module.  Meant to be used with the
# puppetlabs/apache module.
#
# This uses the a2enmod tool provided by Debian to enable a module with its
# configuration.  Example:
#
#     su_apache::module { "rewrite": ensure => present }
#
# This infrastructure, and therefore this define, is only available on Debian.
#
# $restart_apache: restart the Apache service. Defaults to true; set to
# false for Docker container builds.

define su_apache::module(
  Enum['present', 'absent'] $ensure         = undef,
  Boolean                   $restart_apache = true,
) {
  if ($::operatingsystem != 'debian') and ($::operatingsystem != 'ubuntu') {
    fail("Unsupported su_apache::module operating system ${::operatingsystem}")
  }

  include su_apache::package

  # Set various local variables to keep the exec readable.
  $modsdir = '/etc/apache2/mods-enabled'
  $module  = "${modsdir}/${name}"
  $cmd = $ensure ? {
    present => "a2enmod ${name}",
    absent  => "a2dismod ${name}",
  }
  $condition = $ensure ? {
    present => "-e ${module}.load -o -e ${module}.conf",
    absent  => "! -e ${module}.load -a ! -e ${module}.conf",
  }

  # Enable or disable the module using a2enmod or a2dismod.
  if ($restart_apache) {
    # DO an Apache service restart.
    exec { $cmd:
      command => $cmd,
      path    => '/usr/sbin:/usr/bin',
      require => Package['apache'],
      unless  => "[ $condition ]",
      notify  => Exec['su_apache_reload'],
    }
  } else {
    # DON'T do an Apache service restart.
    exec { $cmd:
      command => $cmd,
      path    => '/usr/sbin:/usr/bin',
      require => Package['apache'],
      unless  => "[ $condition ]",
    }
  }
}
