# Apache configuration for Red Hat.

class su_apache::redhat {
  case $::lsbmajdistrelease {
    '3': {
      file {
        '/etc/httpd/conf.d/ssl-strength.conf':
          source  => 'puppet:///modules/su_apache/etc/apache2/conf.d/ssl-strength',
          require => Package['apache'],
          notify  => Service['apache'];
        '/etc/httpd/conf.d/ssl.conf':
          source  => 'puppet:///modules/su_apache/etc/httpd/conf.d/ssl.conf',
          require => Package['apache'],
          notify  => Service['apache'];
        '/etc/httpd/conf.d/ssl-vhost.conf':
          source  => 'puppet:///modules/su_apache/etc/httpd/conf.d/ssl-vhost.conf.RHEL3-4',
          require => Package['apache'],
          notify  => Service['apache'];
      }
    }
    '4': {
      # Do nothing for now
      # TODO: add ssl.conf for all RHEL4.
      # TODO: add ssl-strength.conf for all RHEL4
    }
    # EL5+ only
    default: {
      file {
        '/etc/httpd/conf.d/trace.conf':
          source  => 'puppet:///modules/su_apache/etc/httpd/conf.d/trace.conf',
          require => Package['apache'],
          notify  => Service['apache'];
        '/etc/httpd/conf.d/ssl.conf':
          source  => 'puppet:///modules/su_apache/etc/httpd/conf.d/ssl.conf',
          require => Package['apache'],
          notify  => Service['apache'];
        '/etc/httpd/conf.d/ssl-vhost.conf':
          source  => 'puppet:///modules/su_apache/etc/httpd/conf.d/ssl-vhost.conf.RHEL5',
          require => Package['apache'],
          notify  => Service['apache'];
        '/etc/httpd/conf.d/ssl-strength.conf':
          source  => 'puppet:///modules/su_apache/etc/apache2/conf.d/ssl-strength',
          require => Package['apache'],
          notify  => Service['apache'];
      }
    }
  }
}
