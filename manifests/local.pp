# This class handles configuration of Apache such that people without root or
# Puppet access can manage the server.

class su_apache::local inherits su_apache {
  include group::webconfig,
          base::sudo

  # Ensure a local.conf file with correct permissions and change the ownership
  # of the logs.
  case $::osfamily {
    'RedHat': {
      file {
        '/etc/httpd/conf.d/local.conf':
          ensure  => file,
          group   => 'webconfig',
          mode    => '0664',
          require => Group['webconfig'];
        '/etc/httpd/conf.d/local.sample':
          content => template('su_apache/local.sample.erb'),
          group   => 'webconfig',
          mode    => '0644',
          require => Group['webconfig'];
        '/var/log/httpd':
          ensure  => directory,
          group   => 'webconfig',
          mode    => '0644',
          require => Group['webconfig'];
        '/var/log/httpd/error_log':
          ensure  => file,
          group   => 'webconfig',
          mode    => '0644',
          require => Group['webconfig'];
        '/var/log/httpd/access_log':
          ensure  => file,
          group   => 'webconfig',
          mode    => '0644',
          require => Group['webconfig'];
      }
    }
    'Debian': {
      # In Debian jessie and later, configuration files are in either
      # sites-available or conf-available. In particular,
      # /etc/apache2/conf.d is no longer used. So, we set permissions on
      # files in /etc/apache2/conf.d/ only for wheezy.
      if ($::lsbdistcodename == 'wheezy') {
        file {
          '/etc/apache2/conf.d/local':
            ensure  => present,
            group   => 'webconfig',
            mode    => '0664',
            require => [ Group['webconfig'], Package['apache'] ];
          '/etc/apache2/conf.d/local.sample':
            group   => 'webconfig',
            mode    => '0644',
            content => template('su_apache/local.sample.erb'),
            require => [ Group['webconfig'], Package['apache'] ];
        }
      }
      file {
        '/var/log/apache2':
          ensure  => directory,
          group   => 'webconfig',
          mode    => '0644',
          require => Group['webconfig'];
        '/var/log/apache2/error.log':
          ensure  => present,
          group   => 'webconfig',
          mode    => '0644',
          require => Group['webconfig'];
        '/var/log/apache2/access.log':
          ensure  => present,
          group   => 'webconfig',
          mode    => '0644',
          require => Group['webconfig'];
      }
    }
    default: {
      fail("Unsupported OS ${::operatingsystem} in su_apache::local")
    }
  }

  # Ensure members of the webconfig group can sudo to control Apache.
  case $::osfamily {
    'Debian': {
      base::textline { '%webconfig      ALL=/etc/init.d/apache2':
        ensure => '/etc/sudoers'
      }
    }
    'RedHat': {
      base::textline { '%webconfig      ALL=/etc/init.d/httpd':
        ensure => '/etc/sudoers'
      }
    }
  }

  # Create a tree for local files that people will hopefully use.
  file {
    '/srv':
      ensure  => directory;
    '/srv/www':
      ensure  => directory,
      group   => 'webconfig',
      mode    => '02775',
      require => Group['webconfig'];
  }
}
