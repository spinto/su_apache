# Install Apache appropriate for Stanford.

class su_apache::install ()
  Boolean $include_webauth = false,
) {
  if ($include_webauth) {
    include su_apache::webauth
  }

  # Install apache
  class { 'apache': }

  # Default syslog filtering and iptables configuration.
  file { '/etc/filter-syslog/apache':
    source => 'puppet:///modules/su_apache/etc/filter-syslog/apache',
  }

  base::iptables::rule { 'web':
    description => 'Allow web access from anywhere',
    protocol    => 'tcp',
    port        => [ 80, 443 ],
  }

}
