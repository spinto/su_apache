# This class assumes that some _other_ class has already declared the
# '/etc/filter-syslog" file resource.

class su_apache::filter_syslog (
  Enum['present', 'absent'] $ensure = 'present',
) {

  if ($ensure == 'present') {
    # PRESENT
    # Default syslog filtering and iptables configuration.
    file { '/etc/filter-syslog/apache':
      ensure  => present,
      source  => 'puppet:///modules/su_apache/etc/filter-syslog/apache',
      require => File['/etc/filter-syslog'],
    }
  } else {
    # ABSENT
    file { '/etc/filter-syslog/apache':
      ensure => absent,
    }
  }

}
