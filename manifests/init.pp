# Renaming everything so that we can more safely use the PuppetLabs apache
# module.

class su_apache (
  Boolean $include_webauth = false,
  Boolean $manage_iptables = true,
)
{
  if ($include_webauth) {
    include webauth
  }

  # Install the Apache package; defines the "apache" package
  # resource used in su_apache::module, su_apache::conf, et al.
  include su_apache::package

  # Name the service apache, which doesn't match the name from either OS but
  # is easier to remember.
  service { 'apache':
    ensure     => running,
    name       => $::osfamily ? {
      'Debian' => 'apache2',
      'RedHat' => 'httpd',
    },
    hasrestart => true,
    hasstatus  => $::osfamily ? {
      'RedHat' => false,
      default  => true,
    },
    status     => $::osfamily ? {
      'RedHat' => 'pidof httpd',
      default  => undef,
    },
    require    => Package['apache'],
  }

  # Force a reload after a configuration change.  The Debian init script
  # does a configtest by default, but the Red Hat one does not, so force Red
  # Hat to do the right thing.
  exec { 'su_apache_reload':
    path        => '/bin:/usr/bin:/sbin:/usr/sbin',
    command     => $::osfamily ? {
      'Debian' => 'service apache2 reload',
      'RedHat' => 'sh -c "apachectl configtest && /etc/init.d/httpd reload"',
    },
    refreshonly => true,
    require     => Package['apache'],
  }

  # Operating-system-specific Apache tweaks.
  case $::osfamily {
    'Debian': { include su_apache::debian }
    'RedHat': { include su_apache::redhat }
    default:  { fail("no support for osfamily ${::osfamily}") }
  }

  include su_apache::filter_syslog

  if ($manage_iptables) {
    base::iptables::rule { 'web':
      description => 'Allow web accesses from anywhere',
      protocol    => 'tcp',
      port        => [ 80, 443 ],
    }
  }
}
