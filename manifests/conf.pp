# Install an Apache configuration fragment.
#
# This is used for general configuration fragments, not module configuration
# (use apache::module) or virtual hosts (use apache::site).  Example:
#
#     su_apache::conf {
#       'common':
#         source    => 'puppet:///modules/s_www/etc/apache2/conf.d/common';
#       'common-extra':
#         content   => template('s_www/common-extra.erb'),
#         directory => 'fragments';
#     }
#
# This has not yet been ported to Red Hat and is currently only available for
# Debian.
#
# Supporting jessie requires significant changes.  The conf.d directory
# is no longer used and the files must be in conf-available and linked
# from conf-enabled.

define su_apache::conf(
  $ensure    = 'present',
  $directory = 'conf.d',
  $source    = undef,
  $content   = undef
) {
  if ($::operatingsystem != 'debian') and ($::operatingsystem != 'ubuntu') {
    fail("Unsupported apache::conf operating system ${::operatingsystem}")
  }

  if ($::lsbdistcodename == 'wheezy') {
    $parent   = "/etc/apache2/${directory}"
    $realname = "${parent}/${name}"

    # Create the containing directory if it doesn't exist.  This is a
    # bit of a hack, but a file resource would conflict with other
    # apache::conf instances in the same directory.
    #
    # This always runs even on ensure => absent since otherwise we get
    # errors from the missing dependency.
    exec { "mkparentdir ${realname}":
      command => "mkdir '${parent}'",
      creates => $parent,
      require => Package['apache'],
    }

    # Install the file.
    file { $realname:
      ensure  => $ensure,
      source  => $source,
      content => $content,
      notify  => Exec['su_apache_reload'],
      require => Exec["mkparentdir ${realname}"],
    }
  } else {
    # For jessie and later the $directory value is ignored.  Everything
    # must be in /etc/apache2/conf-enabled

    if $name =~ /.conf$/ {
      $conf_id = $name
    } else {
      $conf_id = "${name}.conf"
    }
    # Install the file.
    file { "/etc/apache2/conf-available/${conf_id}":
      ensure  => $ensure,
      source  => $source,
      content => $content,
      require => Package['apache'],
      notify  => Exec["a2enconf ${conf_id}"],
    }
    exec { "a2enconf ${conf_id}":
      command     => "a2enconf ${conf_id}",
      path        => '/usr/bin:/usr/sbin',
      refreshonly => true,
      require     => File["/etc/apache2/conf-available/${conf_id}"],
      notify      => Exec['su_apache_reload'],
    }
  }
}
