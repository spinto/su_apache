# su_apache Puppet module

This module is simply a renaming of the old Stanford "apache" module.  We
do this renaming in order that it not conflict with the more widely-used
puppetlabs/apache module in PuppetForge.


