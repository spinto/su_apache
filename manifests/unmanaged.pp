# Unmanaged Apache override class.  Does not try to keep the default Apache
# running.  This allows for manual or alternate script management of the
# Apache service.

class su_apache::unmanaged inherits su_apache {
  Service['apache'] {
    hasstatus => false,
    status    => '/bin/true',
  }
}
