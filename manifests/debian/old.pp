# Releases wheezy and older
class su_apache::debian::old(
  $disable_TLS10RC4 = false,
  $disable_3DES     = false,
){
  exec { 'a2dissite 000-default':
    command => 'a2dissite 000-default',
    onlyif  => 'readlink /etc/apache2/sites-enabled/000-default | grep sites-available/default',
    path    => '/bin:/usr/bin:/usr/sbin',
    require => Package['apache'],
  }

  # Disable weak SSL ciphers.
  file { '/etc/apache2/conf.d/ssl-strength':
    content => template('su_apache/etc/apache2/conf.d/ssl-strength.erb'),
    require => Package['apache'],
    notify  => Service['apache'],
  }

  # Replace the default Apache security configuration file with one that
  # suppresses most information disclosure about the server.
  file { '/etc/apache2/conf.d/security':
    source  => 'puppet:///modules/su_apache/etc/apache2/conf.d/security',
    require => Package['apache'],
    notify  => Service['apache'],
  }

  # Support /server-status for all virtual hosts, but only from localhost.
  su_apache::module { 'status':
    ensure  => present,
    require => File['/etc/apache2/mods-available/status.conf'],
  }
  file { '/etc/apache2/mods-available/status.conf':
    source  => 'puppet:///modules/su_apache/etc/apache2/mods-available/status.conf',
    require => Package['apache'],
    notify  => Service['apache'],
  }
}
