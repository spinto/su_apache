# Releases jessie and newer

class su_apache::debian::new(
  $disable_TLS10RC4 = false,
  $disable_3DES     = false,
){
  # Note: the readlink executable comes from the coreutils package.
  exec { 'a2dissite 000-default.conf':
    command => 'a2dissite 000-default.conf',
    onlyif  => 'readlink /etc/apache2/sites-enabled/000-default.conf',
    path    => '/bin:/usr/bin:/usr/sbin',
    require => Package['apache'],
  }

  # Disable weak SSL ciphers.
  su_apache::conf { 'ssl-strength.conf':
    ensure  => present,
    content => template('su_apache/etc/apache2/conf.d/ssl-strength.erb'),
  }

  # Replace the default Apache security configuration file with one that
  # suppresses most information disclosure about the server.
  su_apache::conf { 'security.conf':
    ensure => present,
    source => 'puppet:///modules/su_apache/etc/apache2/conf.d/security',
    require => Package['apache'],
    notify  => Service['apache'],
  }

  # Support /server-status for all virtual hosts, but only from localhost.
  su_apache::module { 'status':
    ensure  => present,
    require => File['/etc/apache2/mods-available/status.conf'],
  }
  file { '/etc/apache2/mods-available/status.conf':
    source  => 'puppet:///modules/su_apache/etc/apache2/mods-available/status.conf',
    require => Package['apache'],
    notify  => Service['apache'],
  }
}
